/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.graph;

import java.util.List;

/**
 *
 * @author souzaala
 */
public interface Graph {       
    
    int vertices();
    int edges();
    void addEdge(int v, int w);
    List<Integer> adjacents(int v);
}
