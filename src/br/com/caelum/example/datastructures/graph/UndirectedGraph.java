/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.graph;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author souzaala
 */
public class UndirectedGraph implements Graph{
    
    private final int vertices;
    private int edges;
    
    private final List<Integer>[] adjacents;
    
    public UndirectedGraph(int v) {
        if (v < 0) 
            throw new IllegalArgumentException("Number of vertices must be positive");
        
        this.vertices = v;
        this.adjacents = (List<Integer>[]) new List[v];
        
        for (int i = 0; i < v; i++) {
            adjacents[i] = new ArrayList<>();
        }
        
    }    
    
    @Override
    public int vertices() {
        return vertices;
    }

    @Override
    public int edges() {
        return edges;
    }

    @Override
    public void addEdge(int v, int w) {
        validateVertex(v);
        validateVertex(w);
        
        edges++;
        adjacents[v].add(w);
        adjacents[w].add(v);        
    }

    @Override
    public List<Integer> adjacents(int v) {
        validateVertex(v);
        return adjacents[v];
    }
    
    private void validateVertex(int v) {
        if (v < 0 || v > vertices) 
            throw new IndexOutOfBoundsException("Vertex is not between 0 and " + (vertices - 1));
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        
        s.append(vertices)
            .append(" vertices, ")
            .append(edges)
            .append(" edges \n");
        
        for (int v = 0; v < vertices; v++) {
            s.append(v).append(": ");
            for (int w : adjacents[v]) {
                s.append(w).append(" ");
            }
            s.append("\n");
        }
        return s.toString();
    }
    
    
    
    
}
