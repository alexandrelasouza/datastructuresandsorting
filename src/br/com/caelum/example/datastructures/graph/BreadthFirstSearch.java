package br.com.caelum.example.datastructures.graph;

import br.com.caelum.example.datastructures.queue.Queue;

import java.util.List;

/**
 * Created by ale_s on 22/06/2016.
 */
public class BreadthFirstSearch {

    public static void search(Graph g, int source) {
        boolean[] visited = new boolean[g.vertices()];

        Queue<Integer> queue = new Queue<>();
        queue.offer(source);

        while (!queue.isEmpty()) {

            int v = queue.poll();
            if (!visited[v]) {
                visited[v] = true;

                System.out.println(v + " ");

                List<Integer> adjacents = g.adjacents(v);
                for (int i = 0; i < adjacents.size(); i++) {
                    queue.offer(adjacents.get(i));
                }
            }

        }


    }

}
