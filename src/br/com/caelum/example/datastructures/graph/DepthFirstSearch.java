package br.com.caelum.example.datastructures.graph;

import br.com.caelum.example.datastructures.stack.Stack;

import java.util.List;

/**
 * Created by ale_s on 22/06/2016.
 */
public class DepthFirstSearch {

    public static void search(Graph g, int source) {

        boolean[] visited = new boolean[g.vertices()];
        Stack<Integer> stack = new Stack<>();

        stack.push(source);

        while(!stack.isEmpty()) {
            int v = stack.pop();
            if(!visited[v]) {
                visited[v] = true;

                System.out.println(v + " ");

                List<Integer> adjacents = g.adjacents(v);
                for (int i = adjacents.size() - 1; i >= 0; i--) {
                    stack.push(adjacents.get(i));
                }

            }
        }

    }

}
