/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.graph;

/**
 *
 * @author souzaala
 */
public class MainGraph {

    public static void main(String[] args) {
        Graph g = new UndirectedGraph(13);
        
        g.addEdge(0, 5);
        g.addEdge(4, 3);
        g.addEdge(0, 1);
        g.addEdge(9, 12);
        g.addEdge(6, 4);
        g.addEdge(5, 4);
        g.addEdge(0, 2);
        g.addEdge(11, 12);
        g.addEdge(9, 10);
        g.addEdge(0, 6);
        g.addEdge(7, 8);
        g.addEdge(9, 11);
        g.addEdge(5, 3);
        
        System.out.println("Undirected graph: \n\n");
        System.out.println(g.toString());
        
        System.out.println("\n\n\n");
        
        DirectedGraph digraph = new DirectedGraph(13);
        
        digraph.addEdge(4, 2);
        digraph.addEdge(2, 3);
        digraph.addEdge(3, 2);
        digraph.addEdge(6, 0);
        digraph.addEdge(0, 1);
        digraph.addEdge(2, 0);
        digraph.addEdge(11, 12);
        digraph.addEdge(12, 9);
        digraph.addEdge(9, 10);
        digraph.addEdge(9, 11);
        digraph.addEdge(7, 9);
        digraph.addEdge(10, 12);
        digraph.addEdge(11, 4);
        digraph.addEdge(4, 3);
        digraph.addEdge(3, 5);
        digraph.addEdge(6, 8);
        digraph.addEdge(8, 6);
        digraph.addEdge(5, 4);
        digraph.addEdge(0, 5);
        digraph.addEdge(6, 4);
        digraph.addEdge(6, 9);
        digraph.addEdge(7, 6);
        
        System.out.println("Directed graph: \n\n");
        System.out.println(digraph.toString());
        
        System.out.println("\n\n\n");
        System.out.println("Reversed directed graph: \n\n");
        System.out.println(digraph.reverse().toString());


        // Binary tree created for the purpose of visualization
        Graph binaryTree = new DirectedGraph(8);
        binaryTree.addEdge(0, 1);
        binaryTree.addEdge(0, 2);
        binaryTree.addEdge(1, 3);
        binaryTree.addEdge(1, 4);
        binaryTree.addEdge(2, 5);
        binaryTree.addEdge(2, 6);
        binaryTree.addEdge(3, 7);

        System.out.println("Breadth first search using a binary tree based on Graph interface: \n");
        BreadthFirstSearch.search(binaryTree, 1); // Starting on 1, prints 1 3 4 7

        System.out.println("\n\n");

        System.out.println("Depth first search using a binary tree based on Graph interface: \n");
        DepthFirstSearch.search(binaryTree, 1); // Starting on 1, prints 1 3 7 4
        
    }
    
}
