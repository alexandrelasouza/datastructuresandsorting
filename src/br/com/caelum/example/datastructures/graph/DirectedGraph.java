/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.graph;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author souzaala
 */
public class DirectedGraph implements Graph {
    
    private final int vertices;
    private int edges;
    private final List<Integer>[] adjacents;
    private final int[] verticesDirectedTo;

    public DirectedGraph(int vertices) {
        this.vertices = vertices;
        this.adjacents = (List<Integer>[]) new List[vertices];
        this.verticesDirectedTo = new int[vertices];
        
        for (int i = 0; i < vertices; i++) {
            adjacents[i] = new ArrayList<>();
        }
        
    }

    @Override
    public int vertices() {
        return vertices;
    }

    @Override
    public int edges() {
        return edges;
    }

    @Override
    public void addEdge(int v, int w) {
        validateVertex(v);
        validateVertex(w);
        adjacents[v].add(w);
        verticesDirectedTo[w]++;
        edges++;
    }

    @Override
    public List<Integer> adjacents(int v) {
        validateVertex(v);
        return adjacents[v];
    }   
    
    
    private void validateVertex(int v) {
        if (v < 0 || v > vertices) 
            throw new IndexOutOfBoundsException("Vertex is not between 0 and " + (vertices - 1));
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        
        s.append(vertices)
            .append(" vertices, ")
            .append(edges)
            .append(" edges \n");
        
        for (int v = 0; v < vertices; v++) {
            s.append(v).append(": ");
            for (int w : adjacents[v]) {
                s.append(w).append(" ");
            }
            s.append("\n");
        }
        return s.toString();
    }
    
    public DirectedGraph reverse() {
        DirectedGraph reversed = new DirectedGraph(vertices);
        for (int v = 0; v < vertices; v++) {
            for (int w : adjacents(v)) {
                reversed.addEdge(w, v);
            }
        }
        
        return reversed;
    }
    
    public int getNumberOfVerticesDirectedTo(int v) {
        validateVertex(v);
        return verticesDirectedTo[v];
    }
    
    public int getNumberOfAdjacentsFrom(int v) {
        validateVertex(v);
        return adjacents[v].size();
    }
    
    
        
}
