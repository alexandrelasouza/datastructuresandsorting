package br.com.caelum.example.datastructures.tree;


public class DepthFirstSearch {

    public static boolean search(Node root, int valueToSearch) {

        if (root == null) {
            return false;
        }

        if (root.getData() == valueToSearch) return true;

        boolean found = search(root.getLeft(), valueToSearch);

        if (!found) {
            found = search(root.getRight(), valueToSearch);
        }

        return found;

    }

}
