package br.com.caelum.example.datastructures.tree;

/**
 * Created by ale_s on 19/05/2016.
 */
public interface SymbolTable<K,V> {

    void put(K key, V value);
    V get(K key);
    void remove(K key);
    boolean contains(K key);
    boolean isEmpty();
    int size();

}
