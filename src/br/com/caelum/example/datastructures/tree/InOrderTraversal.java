package br.com.caelum.example.datastructures.tree;

import br.com.caelum.example.datastructures.stack.Stack;

/**
 * In-Order traversal follows this order:
 * - First the left subtree
 * - The node
 * - Then the right subtree
 */
public class InOrderTraversal {

    public static void traverse(Node root) {

        if (root == null) return;
        traverse(root.getLeft());
        System.out.println(root.getData());
        traverse(root.getRight());

    }

    public static void traverseIterative(Node node) {

        Stack<Node> stack = new Stack<>();

        while (!stack.isEmpty() || node != null) {

            if (node != null) {
                stack.push(node);
                node = node.getLeft();
            } else {
                node = stack.pop();
                System.out.println(node.getData());
                node = node.getRight();
            }

        }

    }

}
