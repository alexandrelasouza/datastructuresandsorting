package br.com.caelum.example.datastructures.tree;

import java.util.LinkedList;
import java.util.Queue;

public class MainTree {

    public static void main(String[] args) {

        Integer[] n = {4, 5, 1, 2, 10, 3, -1, 5, -2, 50, -42, 9, 150, -200};

        BinarySearchTree<Integer, Integer> tree = new BinarySearchTree<>();

        for (int i = 0; i < n.length; i++) {
            tree.put(n[i], i);
        }

        System.out.println("Min key: " + tree.getMinimum());
        System.out.println("Max key: " + tree.getMaximum());


        Node root = new Node(5);
        root.setLeft(new Node(10));
        root.setRight(new Node(15));
        root.getLeft().setLeft(new Node(20));
        root.getLeft().getLeft().setLeft(new Node(50));
        root.getLeft().setRight(new Node(25));
        root.getRight().setLeft(new Node(30));
        root.getRight().setRight(new Node(35));

        System.out.println("Breadth first. Found: " + BreadthFirstSearch.search(root, 500));

        clearState(root);

        System.out.println("Depth first. Found: " + DepthFirstSearch.search(root, 150));

        clearState(root);


        System.out.println("\n\n");

        System.out.println("Inorder traversal: ");
        InOrderTraversal.traverse(root);
        System.out.println("\n\n");
        InOrderTraversal.traverseIterative(root);
        System.out.println("\n\n");


        System.out.println("PreOrder traversal: ");
        PreOrderTraversal.traverse(root);
        System.out.println("\n\n");
        PreOrderTraversal.traverseIterative(root);
        System.out.println("\n\n");



        System.out.println("PostOrder traversal: ");
        PostOrderTraversal.traverse(root);
        System.out.println("\n\n");
        PostOrderTraversal.traverseIterative(root);
        System.out.println("\n\n");


    }

    private static void clearState(Node root) {
        Queue<Node> nodeQueue = new LinkedList<>();
        nodeQueue.offer(root);

        while (!nodeQueue.isEmpty()) {
            Node n = nodeQueue.remove();

            n.setVisited(false);

            if (n.getLeft() != null) {
                nodeQueue.offer(n.getLeft());
            }

            if (n.getRight() != null) {
                nodeQueue.offer(n.getRight());
            }
        }
    }

}


