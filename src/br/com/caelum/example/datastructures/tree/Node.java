package br.com.caelum.example.datastructures.tree;


public class Node {

    private int data;
    private Node left;
    private Node right;
    private boolean visited;

    public Node(int data) {
        this.data = data;
        this.visited = false;
    }

    public int getData() {
        return data;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }
}
