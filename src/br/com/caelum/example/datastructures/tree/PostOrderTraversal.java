package br.com.caelum.example.datastructures.tree;

import br.com.caelum.example.datastructures.stack.Stack;

/**
 * In-Order traversal follows this order:
 * - First the left subtree
 * - Then the right subtree
 * - The node
 */
public class PostOrderTraversal {

    public static void traverse(Node root) {

        if (root == null) return;
        traverse(root.getLeft());
        traverse(root.getRight());
        System.out.println(root.getData());

    }

    public static void traverseIterative(Node node) {

        Stack<Node> stack = new Stack<>();
        Node lastNodeVisited = null;

        while(!stack.isEmpty() || node != null) {
            if (node != null) {
                stack.push(node);
                node = node.getLeft();
            } else {
                Node peekNode = stack.peek();

                // if right child exists and traversing node
                // from left child, then move right
                if (peekNode.getRight() != null && lastNodeVisited != peekNode.getRight()) {
                    node = peekNode.getRight();
                } else {
                    System.out.println(peekNode.getData());
                    lastNodeVisited = stack.pop();
                }

            }
        }

    }

}
