package br.com.caelum.example.datastructures.tree;

import java.util.NoSuchElementException;

/**
 * Created by ale_s on 19/05/2016.
 */
public class BinarySearchTree<K extends Comparable<K>,V> implements SymbolTable<K,V> {

    private Node root;

    private class Node {

        private K key;
        private V value;

        private Node leftChild;
        private Node rightChild;

        private int subTreeSize;

        public Node(K key, V value, int n) {
            this.key = key;
            this.value = value;
            this.subTreeSize = n;
        }

    }


    @Override
    public void put(K key, V value) {
        if (key == null) throw new IllegalArgumentException("Key is null");
        if (value == null) {
            remove(key);
            return;
        }

        root = put(root, key, value);

    }

    private Node put(Node node, K key, V value) {

        if (node == null) return new Node(key, value, 1);

        int cmp = key.compareTo(node.key);

        if (cmp < 0)
            node.leftChild = put(node.leftChild, key, value);

        else if (cmp > 0)
            node.rightChild = put(node.rightChild, key, value);

        else if (node.value == value)
            node.value = value; // if it has the same key and value, rewrite value

        node.subTreeSize = 1 + size(node.leftChild) + size(node.rightChild);

        return node;


    }

    @Override
    public V get(K key) {
        if (key == null) throw new IllegalArgumentException("Key is null");

        return get(root, key);

    }

    private V get(Node node, K key) {

        if (node == null) return null;

        int cmp = key.compareTo(node.key);

        if (cmp < 0)
            return get(node.leftChild, key);

        else if (cmp > 0)
            return get(node.rightChild, key);

        else
            return node.value;
    }

    public void removeMin() {
        if (isEmpty()) throw new NoSuchElementException("Tree is empty");

        root = removeMin(root);
    }

    private Node removeMin(Node node) {

        if (node.leftChild == null) return node.rightChild;

        node.leftChild = removeMin(node.leftChild);
        node.subTreeSize = size(node.leftChild) + size(node.rightChild) + 1;

        return node;

    }

    public void removeMax() {
        if (isEmpty()) throw new NoSuchElementException("Tree is empty");

        root = removeMax(root);

    }

    private Node removeMax(Node node) {

        if (node.rightChild == null) return node.leftChild;

        node.rightChild = removeMax(node.rightChild);
        node.subTreeSize = size(node.leftChild) + size(node.rightChild) + 1;

        return node;

    }

    @Override
    public void remove(K key) {
        if (key == null) throw new IllegalArgumentException("Key is null");

        root = remove(root, key);

    }



    private Node remove(Node node, K key) {

        if (node == null) return null;

        int cmp = key.compareTo(node.key);

        if (cmp < 0)
            node.leftChild = remove(node.leftChild, key);
        else if (cmp > 0)
            node.rightChild = remove(node.rightChild, key);
        else {

            if (node.rightChild == null) return node.leftChild;
            if (node.leftChild == null) return node.rightChild;

            Node t = node;
            node = getMinimum(t.rightChild);
            node.rightChild = removeMin(t.rightChild);
            node.leftChild = t.leftChild;
        }

        node.subTreeSize = size(node.leftChild) + size(node.rightChild) + 1;

        return node;

    }

    public K getMinimum() {
        if (isEmpty()) throw new NoSuchElementException("Tree is empty");

        return getMinimum(root).key;
    }

    private Node getMinimum(Node node) {
        if (node.leftChild == null) return node;

        return getMinimum(node.leftChild);

    }

    public K getMaximum() {
        if (isEmpty()) throw new NoSuchElementException("Tree is empty");

        return getMaximum(root).key;
    }

    private Node getMaximum(Node node) {
        if (node.rightChild == null) return node;

        return getMaximum(node.rightChild);

    }

    @Override
    public boolean contains(K key) {
        return get(key) != null;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public int size() {
        return size(root);
    }

    private int size(Node x) {
        if (x == null) return 0;
        else return x.subTreeSize;
    }
}
