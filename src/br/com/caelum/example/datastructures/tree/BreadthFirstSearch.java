package br.com.caelum.example.datastructures.tree;

import java.util.LinkedList;
import java.util.Queue;


public class BreadthFirstSearch {

    public static boolean search(Node root, int valueToSearch) {

        if (root == null)
            return false;

        Queue<Node> nodeQueue = new LinkedList<>();
        nodeQueue.offer(root);

        while(!nodeQueue.isEmpty()) {
            Node n = nodeQueue.remove();

            if (n.getData() == valueToSearch) {
                return true;
            }

            n.setVisited(true);

            if (n.getLeft() != null) {
                nodeQueue.offer(n.getLeft());
            }

            if (n.getRight() != null) {
                nodeQueue.offer(n.getRight());
            }
        }

        return false;

    }

}
