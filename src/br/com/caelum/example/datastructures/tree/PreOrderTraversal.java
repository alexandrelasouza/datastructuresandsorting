package br.com.caelum.example.datastructures.tree;

import br.com.caelum.example.datastructures.stack.Stack;

/**
 * Pre-Order traversal follows this order:
 * - First the node
 * - Then the left subtree
 * - Then the right subtree
 */
public class PreOrderTraversal {

    public static void traverse(Node root) {

        if (root == null) return;
        System.out.println(root.getData());
        traverse(root.getLeft());
        traverse(root.getRight());

    }

    // Default implementation of DepthFirstSearch
    public static void traverseIterative(Node node) {

        if (node == null) return;

        Stack<Node> stack = new Stack<>();

        stack.push(node);

        while (!stack.isEmpty()) {
            node = stack.pop();
            System.out.println(node.getData());

            if (node.getRight() != null)
                stack.push(node.getRight());

            if (node.getLeft() != null)
                stack.push(node.getLeft());

        }

    }

}
