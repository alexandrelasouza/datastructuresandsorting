/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.linkedlist;

/**
 *
 * @author souzaala
 */
public class MainLinkedList {
    
    public static void main(String[] args) {
        
        LinkedList<String> list = new LinkedList<>();
        
        list.add("Rafael");
        list.add("Paulo");                               
        
        list.removeLast();
        list.removeLast();
        
        System.out.println(list);                        
        
    }
    
}
