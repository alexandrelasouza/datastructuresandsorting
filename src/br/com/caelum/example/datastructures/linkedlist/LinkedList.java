/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.linkedlist;

/**
 *
 * @author souzaala
 * @param <T>
 */
public class LinkedList<T> {

    private Node first;
    private Node last;
    private int size = 0;

    public void addAtBeginning(T element) {

        if (size == 0) {
            Node newCell = new Node(element);
            first = newCell;
            last = newCell;
        } else {
            Node newCell = new Node(first, element);
            first.setPrevious(newCell);
            first = newCell;
        }

        size++;
    }

    public void add(T element) {

        if (size == 0) {
            addAtBeginning(element);
        } else {
            Node newCell = new Node(element);
            newCell.setPrevious(last);
            last.setNext(newCell);
            last = newCell;
            size++;
        }

    }

    public void add(int position, T element) {

        if (position == size) {
            add(element);
            return;
        }

        if (position == 0) {
            addAtBeginning(element);
            return;
        }

        Node previousCell = getCell(position - 1);
        Node current = getCell(position);
        Node newCell = new Node(previousCell.getNext(), element);

        current.setPrevious(newCell);
        newCell.setPrevious(previousCell);

        previousCell.setNext(newCell);

        size++;
    }

    public T get(int position) {
        return getCell(position).getElement();
    }

    public void remove(int position) {

        if (position == 0) {
            removeFirst();
        } else if (position == size - 1) {
            removeLast();
        } else {

            Node previousCell = getCell(position - 1);
            Node current = previousCell.getNext();
            Node nextCell = current.getNext();

            previousCell.setNext(nextCell);
            nextCell.setPrevious(previousCell);

            size--;
        }
    }

    public void removeFirst() {

        if (size == 0) {
            throw new IndexOutOfBoundsException();
        }

        first = first.getNext();
        size--;

        if (first != null) {
            first.setPrevious(null);
        }

        if (size == 0) {
            this.last = null;
        }

    }

    public void removeLast() {

        if (size == 0) {
            throw new IndexOutOfBoundsException();
        }

        if (size == 1) {
            removeFirst();
            return;
        }

        Node cellBeforeLast = last.getPrevious();
        cellBeforeLast.setNext(null);
        last = cellBeforeLast;

        size--;

    }

    private Node getCell(int position) {

        if (position < 0 || position > size) {
            throw new IndexOutOfBoundsException();
        }

        Node current = first;

        for (int i = 0; i < position; i++) {
            current = current.getNext();
        }

        return current;

    }

    public int size() {
        return size;
    }

    public boolean contains(T element) {

        Node current = this.first;

        while (current != null) {
            if (current.getElement().equals(element)) {
                return true;
            }

            current = current.getNext();
        }

        return false;
    }

    @Override
    public String toString() {

        if (size == 0) {
            return "[]";
        }

        StringBuilder stringRepresentation = new StringBuilder("[");

        Node current = this.first;

        for (int i = 0; i < size - 1; i++) {
            stringRepresentation.append(current.getElement())
                    .append(", ");

            if (current.getNext() != null) {
                current = current.getNext();
            }
        }

        // Last element
        // Obs: could be null if list is empty        
        stringRepresentation.append(current.getElement());

        stringRepresentation.append("]");

        return stringRepresentation.toString();
    }

    private class Node {

        private Node next;
        private Node previous;
        private T element;

        public Node(Node next, T element) {
            this.next = next;
            this.element = element;
        }

        public Node(T element) {
            this.element = element;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        public Node getPrevious() {
            return previous;
        }

        public void setPrevious(Node previous) {
            this.previous = previous;
        }

        public T getElement() {
            return element;
        }

    }
}
