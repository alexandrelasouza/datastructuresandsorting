/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.stack;

import java.util.LinkedList;

/**
 *
 * @author souzaala
 * @param <T>
 */
public class Stack<T> {
    
    private final LinkedList<T> list;
    
    public Stack() {
        this.list = new LinkedList<>();
    }
    
    public void push(T element) {
        list.add(element);        
    }
    
    public T pop() {
        return list.removeLast();
    }

    public T peek() { return list.getLast(); };
    
    public boolean isEmpty() {
        return list.isEmpty();
    }
    
}
