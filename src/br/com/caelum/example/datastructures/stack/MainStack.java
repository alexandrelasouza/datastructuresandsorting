/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.stack;

/**
 *
 * @author souzaala
 */
public class MainStack {

    public static void main(String[] args) {
        
        String message = cryptDecrypt("Uma mensagem confidencial");
        System.out.println(message);
        
        message = cryptDecrypt(message);
        System.out.println(message);
        
    }
    
    private static String cryptDecrypt(String valueToEncrypt) {
        
        Stack<Character> characters = new Stack<>();
        
        for (char c : valueToEncrypt.toCharArray()) {
            characters.push(c);
        }
        
        StringBuilder builder = new StringBuilder();
        
        while(!characters.isEmpty()) {
            builder.append(characters.pop());
        }                        
        
        return builder.toString();
        
        /* 
            Same operation implemented without the use of a Stack:
        
            StringBuilder builder = new StringBuilder(valueToEncrypt);
            builder.reverse().toString();
        */
        
    }
    
}
