/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.sorting;

import java.util.Arrays;

/**
 *
 * @author souzaala
 */
public class MainSorting {

    
    public static void main(String[] args) {
        Integer[] n = {4, 5, 1, 2, 10, 3, -1, 5, -2, 50, -42, 9, 100, -65};
//        SelectionSort.sort(n);
//        InsertionSort.sort(n);
//        MergeSort.sort(n);
//        ShellSort.sort(n);
        QuickSort.sort(n);
//        QuickSort3Way.sort(n);
//        HeapSort.sort(n);
//        BubbleSort.sort(n);
//        BubbleSort.sortSimple(n);
        System.out.println(Arrays.toString(n));

        int[] arr = {2,1,0,8,2,2,2,2,3,0,3,5,1,2,3,4};

        int[] sorted = IntegerCountingSort.sort(arr); // only for single digits
        System.out.println(Arrays.toString(sorted));

        String[] strArr = {"zfc", "abc", "abd", "mab", "maa"};
        RadixSort.sort(strArr, 3);
        System.out.println(Arrays.toString(strArr));

        int[] intArr = {129, 128, 127, 124, 1, 125, 123, 130, 13};
        RadixSort.sort(intArr);
        System.out.println(Arrays.toString(intArr));
    }
    
}
