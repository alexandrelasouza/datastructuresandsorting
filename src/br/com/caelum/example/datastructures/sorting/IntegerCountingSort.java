package br.com.caelum.example.datastructures.sorting;

import java.util.Arrays;

/**
 * Created by ale_s on 19/06/2016.
 */
public class IntegerCountingSort {

    public static int[] sort(int[] arr) {

        int[] counter = new int[10];

        for (int i : arr) {
            counter[i]++;
        }

        int[] positions = Arrays.copyOf(counter, counter.length);

        for (int i = 1; i < positions.length; i++) {
            positions[i] += positions[i-1];
        }

        int[] result = new int[arr.length];

        for (int i = arr.length - 1; i >= 0; i--) {
            positions[arr[i]]--;
            int position = positions[arr[i]];
            result[position] = arr[i];
        }

        return result;

    }

}
