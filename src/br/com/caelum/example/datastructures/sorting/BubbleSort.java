/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.sorting;

/**
 *
 * @author souzaala
 */
public class BubbleSort {

    public static void sort(Comparable[] arr) {
        boolean swapped;       
        
        for(int i=0; i < arr.length; i++) 
        {  
            swapped = false;
            
            for( int j=arr.length - 1; j > i ; j--) 
            {
                if(SortingUtils.isLess(arr[j], arr[j-1]))  
                {
                    SortingUtils.swap(arr, j, j-1);
                    swapped = true;
                }
            }
            
            if (!swapped) break;
            
        }
        
//        for(int i = 0; i < arr.length; i++) {
//            
//            for (int j = arr.length - 1; j > i; j--) {
//                if (!SortingUtils.isLess(j, j-1)); {
//                    SortingUtils.swap(arr, j, j-1);
//                }
//            }
//            
//        }

    }
    
    public static void sortSimple(Comparable[] arr) {
        
        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (!SortingUtils.isLess(arr[j], arr[j+1])) {
                    SortingUtils.swap(arr, j, j+1);
                }
            }
        }
        
    }
    
}