/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.sorting;

/**
 *
 * @author souzaala
 */
public class SelectionSort {
    
    public static void sort(Comparable[] arr) {
        int minIndex = 0;
        
        for (int i = 0; i < arr.length; i++) {            
            
            minIndex = i;
            
            for (int j = i+1; j < arr.length; j++) {
                if(SortingUtils.isLess(arr[j], arr[minIndex])) {
                    minIndex = j;
                }
            }
            
            SortingUtils.swap(arr, minIndex, i);
            
        }
        
    }        
    
}
