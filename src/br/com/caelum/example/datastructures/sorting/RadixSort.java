package br.com.caelum.example.datastructures.sorting;

import java.util.Arrays;

/**
 * Created by ale_s on 19/06/2016.
 */
public class RadixSort {

    private static final int BITS_PER_BYTE = 8;

    public static void sort(String[] arr, int stringSize) {

        int N = arr.length;
        int R = 256; // extend ASCII alphabet size
        String[] aux = new String[N];

        for (int charPos = stringSize - 1; charPos >= 0; charPos--) {

            int[] buckets = new int[R];

            // compute frequency
            for (String str : arr) {
                buckets[str.charAt(charPos) + 1]++;
            }

            // compute cumulates
            for (int r = 1; r < R; r++) {
                buckets[r] += buckets[r - 1];
            }

            // move data
            for (String str : arr) {
                aux[buckets[str.charAt(charPos)]++] = str;
            }

            // Copy back
            System.arraycopy(aux, 0, arr, 0, arr.length);

        }

    }

    public static void sort(int[] arr) {
        int N = arr.length;

        for (int digit = 0; digit < 3; digit++) {
            int power = (int) Math.pow(10, digit + 1);

            int z[][] = new int[N][10];
            int n[] = new int[10];

            for (int num : arr) {
                int index = (num % power) / (power / 10);
                z[n[index]][index] = num;
                n[(num % power) / (power / 10)]++;
            }

            int c = 0;

            for (int i = 0; i < 10; i++) {

                for (int j = 0; j < N; j++) {
                    if (j < n[i]) {
                        arr[c] = z[j][i];
                        c++;
                    } else {
                        break;
                    }
                }
            }

        }
    }

}
