/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.sorting;

/**
 *
 * @author souzaala
 */
public class InsertionSort {
    
    public static void sort(Comparable[] arr) {
        sort(arr, 0);
    }
    
    public static void sort(Comparable[] arr, int start) {
        for(int i = start + 1; i < arr.length; i++) {
            for (int j = i; j > 0 && SortingUtils.isLess(arr[j], arr[j-1]); j--) {                
                    SortingUtils.swap(arr, j, j-1);                
            }
        }
    }        
    
}
