package br.com.caelum.example.datastructures.sorting;


/**
 * Created by ale_s on 18/05/2016.
 */
public class QuickSort {

    public static void sort(Comparable[] arr) {
        sort(arr, 0, arr.length - 1);
    }

    private static void sort(Comparable[] arr, int lo, int hi) {

        if (hi <= lo) return;
        int j = partition(arr, lo, hi);
        sort(arr, lo, j-1);
        sort(arr, j+1, hi);

    }

    private static int partition(Comparable[] arr, int lo, int hi) {
        Comparable v = arr[lo]; // takes element on the left position
        int i = lo;
        int j = hi + 1; // tracks array position to swap


        while(true) {

            // find item on lo to swap
            while(SortingUtils.isLess(arr[++i], v))
                if (i == hi) break;

            // find item on hi to swap
            while(SortingUtils.isLess(v, arr[--j]))
                if (j == lo) break;

            // check if pointers cross
            if (i >= j) break;

            SortingUtils.swap(arr, i, j);

        }

        SortingUtils.swap(arr, lo, j); // puts partitioning item v in its final position

        return j;

    }

}
