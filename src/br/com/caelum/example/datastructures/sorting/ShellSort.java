/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.sorting;

/**
 *
 * @author souzaala
 */
public class ShellSort {

    public static void sort(Comparable[] arr) {
        int h = 1;
        int length = arr.length;
        
        while (h < length/3) h = 3*h + 1;
        
        while (h > 0) {            
            
            for (int i = h; i < length; i++) {
                for (int j = i; j >= h && SortingUtils.isLess(arr[j], arr[j-h]); j -= h) {
                    SortingUtils.swap(arr, j, j-h);
                }
            }          
            
            h = h/3;
            
        }
        
    }
    
}
