package br.com.caelum.example.datastructures.sorting;

/**
 * Created by ale_s on 18/05/2016.
 */
public class QuickSort3Way {

    public static void sort(Comparable[] arr) {
        sort(arr, 0, arr.length - 1);
    }

    private static void sort(Comparable[] arr, int lo, int hi) {

        if (hi <= lo) return;
        int lt = lo, gt = hi;

        Comparable v = arr[lo];
        int i = lo;

        while (i <= gt) {
            int cmp = arr[i].compareTo(v);

            if (cmp < 0)
                SortingUtils.swap(arr, lt++, i++);
            else if (cmp > 0)
                SortingUtils.swap(arr, i, gt--);
            else
                i++;
        }

        sort(arr, lo, lt - 1);
        sort(arr, gt + 1, hi);

    }

}
