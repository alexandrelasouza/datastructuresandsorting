/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.sorting;

/**
 *
 * @author souzaala
 */
public class MergeSort {        
    
    public static void sort(Comparable[] arr) {
        Comparable[] aux = new Comparable[arr.length];
        sort(arr, aux, 0, arr.length - 1);
    }
    
    private static void sort(Comparable[] arr, Comparable[] aux, int start, int end) {                
        
        if (end <= start) return;     
        
        int mid = start + (end - start) / 2;                
        
        sort(arr, aux, start, mid);
        sort(arr, aux, mid + 1, end);                                
        merge(arr, aux, start, mid, end);
    }
    
    private static void merge(Comparable[] arr, Comparable[] aux, int start, int mid, int end) {
        
        // Copy to aux
        for (int i = start; i <= end; i++) {
            aux[i] = arr[i];
        }
        
        int i = start;
        int j = mid + 1;
        
        // merge it back to arr
        for (int k = start; k <= end; k++) {
            if (i > mid) {
                arr[k] = aux[j];
                j++;
            } else if (j > end) {
                arr[k] = aux[i];
                i++;
            } else if (SortingUtils.isLess(aux[j], aux[i])) {
                arr[k] = aux[j];
                j++;
            } else {
                arr[k] = aux[i];
                i++;
            }
        }
        
    }    
    
}
