/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.sorting;

/**
 *
 * @author souzaala
 */
public class HeapSort {

    public static void sort(Comparable[] arr) {

        int n = arr.length;
        
        for (int i = n / 2; i >= 1; i--) {
            siftDown(arr, i, n);
        }
        
        while(n > 1) {
            swap(arr, 1, n--);
            siftDown(arr, 1, n);
        }
        
    }
    
    private static void siftDown(Comparable[] arr, int parent, int N) {
        while(2*parent <= N) {
            int child = 2*parent;
            
            // if right child is lesser than left child            
            if (child < N && isLess(arr, child, child+1)) 
                child++;
            
            if (!isLess(arr, parent, child))
                break;
            
            
            swap(arr, parent, child);
                        
            parent = child;
        }
    }
    
    private static boolean isLess(Comparable[] arr, int i, int j) {
        return arr[i-1].compareTo(arr[j-1]) < 0;
    }
    
    private static void swap(Comparable[] arr, int indexA, int indexB) {
        Comparable t = arr[indexA-1];
        arr[indexA-1] = arr[indexB-1];
        arr[indexB-1] = t;
    }
    
}
