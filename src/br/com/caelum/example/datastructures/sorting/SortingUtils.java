/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.sorting;

/**
 *
 * @author souzaala
 */
public class SortingUtils {
    
    public static boolean isLess(Comparable a, Comparable b) {
        return a.compareTo(b) < 0;
    }

    public static boolean isLessOrEqual(Comparable a, Comparable b) {
        return a.compareTo(b) <= 0;
    }
    
    public static void swap(Comparable[] arr, int indexA, int indexB) {
        Comparable t = arr[indexB];
        arr[indexB] = arr[indexA];
        arr[indexA] = t;
    }
    
}
