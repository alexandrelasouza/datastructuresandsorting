/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.priorityqueue;

import java.util.Comparator;

/**
 *
 * @author souzaala
 */
public class MainPriorityQueue {
    
    public static void main(String[] args) {
        Integer[] n = {4, 5, 1, 2, 10, 3, -1, 5, -2, 50, -42, 9, 100, -65};
        
        Comparator<Integer> comparator = new Comparator<Integer>() {

            @Override
            public int compare(Integer o1, Integer o2) {
                if(o1 < o2) {
                    return -1;
                } else if (o1.equals(o2)) {
                    return 0;
                } else {
                    return 1;
                }
            }
        };
        
        MaxPriorityQueue<Integer> maxPq = new MaxPriorityQueue<>(comparator);
        MinPriorityQueue<Integer> minPq = new MinPriorityQueue<>(comparator);
        
        for (Integer i : n) {
            maxPq.offer(i);
            minPq.offer(i);
        }
        
        int size = maxPq.size();
        System.out.println("MAX Priority Queue: ");
        for (int i = 0; i < size; i++) {
            System.out.println(maxPq.poll());
        }
        
        System.out.println("\n\n");
        
        size = minPq.size();
        System.out.println("MIN Priority Queue: ");
        for (int i = 0; i < size; i++) {
            System.out.println(minPq.poll());
        }
                
    }
    
}
