/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.priorityqueue;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author souzaala
 * @param <T>
 */
public class MaxPriorityQueue<T> {
    
    private List<T> entries;
    private Comparator<T> comparator;
    
    private static final int DEFAULT_INITIAL_CAPACITY = 10;        
    
    public MaxPriorityQueue(Comparator<T> comparator) {
        this(comparator, DEFAULT_INITIAL_CAPACITY);
    }
    
    public MaxPriorityQueue(Comparator<T> comparator, int capacity) {
        this.entries = new ArrayList<>(capacity);
        this.comparator = comparator;
    }
    
    public void offer(T element) {
        entries.add(element);
        siftUp(entries.size() - 1);
    }
    
    public T poll() {
        
        T v = entries.get(0);
        
        T last = entries.remove(entries.size() - 1);
        
        if (last != null && !entries.isEmpty()) {
            entries.set(0, last);
            siftDown(0);
        }
        
        return v;
    }
    
    public int size() {
        return entries.size();
    }
    
    private void siftUp(int child) {
        
        int parent = (child-1)/2;
        
        // if child is higher than the parent, push child up and parent down
        while(child >= 1 && comparator.compare(entries.get(parent), entries.get(child)) < 0) {
            swap(child, parent);
            child = parent;
            parent = (child-1)/2;
        }
    }
    
    private void siftDown(int parent) {
        while(2*parent + 1 < entries.size()) {
            int child = 2*parent + 1;
            
            // if right child is higher than left child            
            if (child < entries.size() && child + 1 < entries.size() && comparator.compare(entries.get(child), entries.get(child+1)) < 0) 
                child++;
            
            if (comparator.compare(entries.get(parent), entries.get(child)) >= 0) // ordered
                break;
            
            // if parent is lower than the child, push parent down and child up
            if (comparator.compare(entries.get(parent), entries.get(child)) < 0) {
                swap(parent, child);
            }
            
            parent = child;
        }
    }
    
    private void swap(int indexA, int indexB) {
        T t = entries.get(indexB);
        entries.set(indexB, entries.get(indexA));
        entries.set(indexA, t);
    }
    
}
