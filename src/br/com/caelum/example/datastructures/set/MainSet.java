/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.set;

/**
 *
 * @author souzaala
 */
public class MainSet {
    
    public static void main(String[] args) {
        
        long inicio = System.currentTimeMillis();
        
        // Remove the "checkSize()" method call to see performance difference
        Set<String> conjunto = new Set<>();
        
        for (int i = 0; i < 50000; i++) {
            conjunto.add("palavra" + i);
        }
        
        for (int i = 0; i < 50000; i++) {
            conjunto.contains("palavra" + i);
        }
        long fim = System.currentTimeMillis();
        
        System.out.println("Tempo: " + (fim - inicio) + "ms");
        
    }
    
}
