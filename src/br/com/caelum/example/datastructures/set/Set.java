/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.set;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author souzaala
 */
public class Set<T> {
    
    private final List<List<T>> table;
    private int size;
    
    public Set() {
        this.table = new ArrayList<>();
        this.size = 0;
        
        for (int i = 0; i < 26; i++) {
            this.table.add(new LinkedList<>());
        }       
    }
    
    public void add(T element) {
        
        if (!this.contains(element)) {
            checkSize();
            int tableIndex = hash(element);
            this.table.get(tableIndex).add(element);
            this.size++;
        }
    }
    
    public void remove(T element) {
        
        if (this.contains(element)) {
            int tableIndex = hash(element);
            this.table.get(tableIndex).remove(element);
            this.size--;
            checkSize();
        }
    }
    
    public boolean contains(T element) {
        int tableIndex = hash(element);
        return this.table.get(tableIndex).contains(element);
    }
    
    public int size() {
        return this.size;
    }
 
    public List<T> getAll() {
        List<T> elements = new ArrayList<>();
        
        for (List<T> l : this.table) {
            elements.addAll(l);
        }
        
        return elements;
    }
 
    /**
     * USE THIS IN ANY OTHER OPERATION OF THIS CLASS
     */
    private int hash(T element) {                
        int hash = element.hashCode();
        return Math.abs(hash % table.size());
    }    
    
    private void checkSize() {
        int currentCapacity = this.table.size();
        double loadFactor = (double) this.size() / currentCapacity;
        
        if (loadFactor > 0.75) {
            ensureSize(currentCapacity * 2);
        } else if (loadFactor < 0.25) {
            ensureSize(Math.max(currentCapacity/2, 10));
        }
        
    }
    
    private void ensureSize(int newCapacity) {                
        
        List<T> elements = this.getAll();
        this.table.clear();
        
        for (int i = 0; i < newCapacity; i++) {
            this.table.add(new LinkedList<>());
        }
        
        for (T element : elements) {
            int tableIndex = hash(element);
            this.table.get(tableIndex).add(element);
        }
        
    }
    
}
