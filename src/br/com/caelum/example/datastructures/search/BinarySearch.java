/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.search;

/**
 *
 * @author souzaala
 */
public class BinarySearch {

    public static int search(Comparable[] arr, Comparable target) {
        
        int lo = 0;
        int hi = arr.length - 1;
        int mid = ((Double) Math.floor((lo + hi)/2)).intValue();
        
        while(lo <= hi) {
            int cmp = target.compareTo(arr[mid]);
            if (cmp == 0) { 
                return mid;
            } else if (cmp == -1) {
                hi = mid - 1;
            } else {
                lo = mid + 1;                
            }
            
            mid = ((Double) Math.floor((lo + hi)/2)).intValue();
            
        }
        
        return -1;
    }
    
    public static int search(Comparable[] arr, Comparable target, int lo, int hi) {
        
        if (lo > hi) return -1;
        
        int mid = ((Double) Math.floor((lo + hi)/2)).intValue();
        
        int cmp = target.compareTo(arr[mid]);
        
        if (cmp == 0)
            return mid;
        else if (cmp == -1) {
            return search(arr, target, lo, mid - 1);
        } else {
            return search(arr, target, mid + 1, hi);
        }
        
        
    }
    
}
