/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.search;

import br.com.caelum.example.datastructures.sorting.ShellSort;

/**
 *
 * @author souzaala
 */
public class MainSearch {

    
    public static void main(String[] args) {
        Integer[] n = {4, 5, 1, 2, 10, 3, -1, 5, -2, 50, -42, 9, 100, -65};
        
        ShellSort.sort(n);
        
        int index = BinarySearch.search(n, 4);
        
        int indexRecursion = BinarySearch.search(n, 4, 0, n.length - 1);
        
        if (index != -1) {
            System.out.println("Value found! Value '" + n[index] + "' is at index: " + index);
        } else {
            System.out.println("Value not found :(");
        }
        
        
        if (indexRecursion != -1) {
            System.out.println("Value found via recursivity! Value '" + n[indexRecursion] + "' is at index: " + indexRecursion);
        } else {
            System.out.println("Value not found via recursivity :(");
        }
        
    }
    
}
