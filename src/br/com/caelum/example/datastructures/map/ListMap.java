/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.map;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author souzaala
 */
// THIS IS NOT EFFECTIVE SINCE FOR EVERY OPERATION WE NEED TO GO THROUGH THE LIST
//
// IGNORE IT AND USE HASHMAP
public class ListMap<K,V> {
    
    private List<MapEntry<K,V>> entries;
    
    public ListMap() {
        this.entries = new ArrayList<>();
    }
    
    public void put(K key, V value) {
        if (!contains(key)) {
            this.entries.add(new MapEntry<>(key, value));
        } else {
            throw new IllegalArgumentException("Key already exists");
        }
    }
    
    public V get(K key) {
        
        for (MapEntry<K,V> m : this.entries) {
            if (m.getKey().equals(key)) {
                return m.getValue();
            }
        }
        
        return null;
    }
    
    public void remove(K key) {
        
        if (contains(key)) {
            for (int i = 0; i < this.entries.size(); i++) {
                if (this.entries.get(i).getKey().equals(key)) {
                    this.entries.remove(i);            
                    break;
                }
            }
        } else {
            throw new IllegalArgumentException("Key does not exists");
        }
    }
    
    public boolean contains(K key) {
        
        for (MapEntry<K,V> m : this.entries) {
            if (m.getKey().equals(key)) {
                return true;
            }
        }
        return false;
    }
    
    public int size() {
        return this.entries.size();
    }
    
    
    
}
