/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.map;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author souzaala
 * @param <K>
 * @param <V>
 */
public class HashMap<K, V> {
    
    private List<List<MapEntry<K,V>>> table;
    private int size;
    
    public HashMap() {
        this.table = new ArrayList<>(100);
        this.size = 0;
        
        for (int i = 0; i < 100; i++) {
            this.table.add(new LinkedList<>());
        }        
        
    }
    
    private int hash(K key) {
        return Math.abs(key.hashCode()) % this.table.size();
    }
    
    public void put(K key, V value) {                                
        
        if(contains(key)) 
            remove(key);
        
        checkSize();
        MapEntry<K,V> entry = new MapEntry<>(key, value);
        int tableIndex = hash(key);
        this.table.get(tableIndex).add(entry);
        this.size++;
        
        
    }
    
    public V get(K key) {
        
        int tableIndex = hash(key);
        List<MapEntry<K,V>> list = this.table.get(tableIndex);
        
        for (MapEntry<K,V> entry : list) {
            if (entry.getKey().equals(key)) {
                return entry.getValue();
            }
        }
        
        throw new IllegalArgumentException("Key does not exist");
    }
    
    public void remove(K key) {
        
        int tableIndex = hash(key);
        List<MapEntry<K,V>> list = this.table.get(tableIndex);

        for(int i = 0; i < list.size(); i++) {
            MapEntry<K,V> entry = list.get(i);
            if (entry.getKey().equals(key)) {
                list.remove(entry);
                this.size--;
                checkSize();
                return;
            }
        }
                    
        throw new IllegalArgumentException("Key does not exists");
        
    }
    
    public boolean contains(K key) {
        
        int index = hash(key);
        List<MapEntry<K,V>> list = this.table.get(index);
        
        for (MapEntry<K,V> entry : list) {
            if (entry.getKey().equals(key)) {
                return true;
            }
        }
        
        return false;
    }
    
    public int size() {
        return this.size;
    }
    
    private void checkSize() {
        int currentCapacity = this.table.size();
        double loadFactor = (double) this.size() / currentCapacity;
        
        if (loadFactor > 0.75) {
            ensureSize(currentCapacity * 2);
        } else if (loadFactor < 0.25) {
            ensureSize(Math.max(currentCapacity/2, 10));
        }
        
    }
    
    public List<MapEntry<K,V>> getAll() {
        
        List<MapEntry<K,V>> list = new LinkedList<>();
        
        for (List<MapEntry<K,V>> l : this.table) {
            list.addAll(l);
        }
        
        return list;
    }
    
    private void ensureSize(int newCapacity) {                
        
        List<MapEntry<K,V>> elements = this.getAll();
        this.table.clear();
        
        for (int i = 0; i < newCapacity; i++) {
            this.table.add(new LinkedList<>());
        }
        
        for (MapEntry<K,V> entry : elements) {
            int tableIndex = hash(entry.getKey());
            this.table.get(tableIndex).add(entry);
        }
        
    }
    
}
