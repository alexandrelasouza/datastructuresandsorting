/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.map;

/**
 *
 * @author souzaala
 */
public class HashMapTimeTest {

    public static void main(String[] args) {

        HashMap<String, Integer> map = new HashMap<>();

        int n = 15000;
        long start = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            map.put("" + i, i);
        }

        for (int i = 0; i < n; i++) {
            map.get("" + i);
        }
        for (int i = 0; i < n; i++) {
            map.contains("" + i);
        }
        for (int i = 0; i < n; i++) {
            map.remove("" + i);
        }

        long end = System.currentTimeMillis();
        System.out.println("Elapsed time: " + (end - start) + "ms");

    }
    
}
