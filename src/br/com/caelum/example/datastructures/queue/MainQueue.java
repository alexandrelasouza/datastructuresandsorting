/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.queue;

/**
 *
 * @author souzaala
 */
public class MainQueue {
    
    private static final int N = 10000;
    
    public static void main(String[] args) {
        long now = System.currentTimeMillis();
        
        // Change LinkedList to ArrayList to see the difference in elapsed time.
        // In this case ArrayList is slower because of its necessity 
        // to resize the underlying array and because for every removal, it shifts 
        // the elements to the left
        Queue<Integer> queue = new Queue<>();
        
        for (int i = 0; i < N; i++) {
            queue.offer(i);
        }
        
        for (int i = 0; i < N; i++) {
            queue.poll();
        }
        
        long afterTest = System.currentTimeMillis();
        
        System.out.println("Elapsed time: " + (afterTest - now) + "ms");
        
    }
    
}
