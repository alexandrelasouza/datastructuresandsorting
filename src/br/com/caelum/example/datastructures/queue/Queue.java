/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.caelum.example.datastructures.queue;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author souzaala
 * @param <T>
 *  
 * 
 */
public class Queue<T> {

    private final List<T> values;
    
    public Queue() {
        this.values = new LinkedList<>();
    }
    
    public Queue(Collection<? extends T> collection) {
        this.values = new LinkedList<>(collection);
    }
    
    public void offer(T value) {
        this.values.add(value);
    }
    
    public T poll() {
        return this.values.remove(0);
    }
    
    public int size() {
        return this.values.size();
    }
    
    public boolean isEmpty() {
        return size() == 0;
    }
    
}
